def welcome() 
    print "Welcome to simple-ruby-calc! Type a command to get started, and type help if you don't know what to do. "
    $command = gets.chomp.downcase.strip
end
def cmd()
    print ">>> "
    $command = gets.chomp.downcase.strip
end
welcome()
while $command != "exit"
    if $command == "eval"
        print "Enter an expression to evaluate (as Ruby code): "
        code = gets.chomp
        e = eval(code) 
        puts e 
        cmd()
    end
    if $command == "add"
        print "Type a number: "
        number1 = gets.chomp.to_f
        print "Now type another number to add to this number: "
        number2 = gets.chomp.to_f
        puts "Converting values to floats..."
        puts "#{number1} + #{number2} = #{number1+number2}; operation successful."
        cmd()
    end
    if $command == "subtract"
        print "Type the minuend (the larger number) of your equation: "
        minuend = gets.chomp.to_f
        print "Now type the subtrahend (the smaller number): "
        subtrahend = gets.chomp.to_f 
        puts "Converting values to floats..."
        puts "#{minuend} - #{subtrahend} = #{minuend-subtrahend}; operation successful."
        cmd()
    end
    if $command == "clear"
        system("clear") || system("cls")
        cmd()
    end
    if $command == "multiply"
        print "Type your first factor: "
        ff = gets.chomp.to_f
        print "Now type your second factor: "
        sf = gets.chomp.to_f
        puts "Converting values to floats..."
        puts "#{ff} * #{sf} = #{ff*sf}; operation successful."
        cmd()
    end
    if $command == "divide"
        print "Type the dividend of the equation: "
        dividend = gets.chomp.to_f
        print "Now type the divisor: "
        divisor = gets.chomp.to_f
        puts "Converting values to floats..."
        puts "#{dividend} / #{divisor} = #{dividend/divisor}; operation successful."
        cmd()
    end
    if $command == "help"
        puts """clear - clear the screen
        divide - divide
        multiply - multiply
        add - add
        subtract - subtract
        eval - evaluate Ruby expressions
        help - this dialog
        exit - exit
        """
        cmd()
    end
    list = ["clear", "divide", "multiply", "add", "subtract", "eval", "help", "exit"]
    if not list.include?($command) then
        puts "Command not recognized, please check spelling."
        cmd()
    end
if $command == 'exit'
    exit
end
end
